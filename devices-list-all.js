var unirest = require('unirest');	// Unirest is a set of lightweight HTTP request client libraries

var user = 'admin1005';			// User name
var pass = 'Namer007$';			// Password

var apiKey='a3zzn79c2escgdembcm2wep2';	// Mashery API key

// Accept windriver.api.mashery.com self-signed certificate
process.env.NODE_TLS_REJECT_UNAUTHORIZED = "0";

// Form the GET request to list all devices
unirest.get('https://api.helixdevicecloud.com/rest/systemmanagement/v1/devices?api_key='+apiKey)

// Form the GET request headers: authorization and accepted encoding
.auth(user, pass)
.headers({'Accept': 'application/json', 'Content-Type': 'application/json'})

// Send the request and process the response from the server
.end(function (response) {

// Print the received JSON items in the response
for(var i=0; i < response.body.data.currentItemCount; i++)
  console.log(response.body.data.items[i]);
});
